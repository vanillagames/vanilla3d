﻿using UnityEngine;
using System.Collections;

namespace Vanilla.ThreeDee {
	public class LookAtAdvanced : LookAtBasic {

		public Axis lookAxis;

		public bool applyLocally;

		public bool doClamping;

		[Range(0,360)]
		public float clampMin, clampMax;

		private void Awake() {
			if (clampMax < clampMin) {
				Warning("ClampMax is lower than ClampMin!");
				clampMax = clampMin + 1.0f;
			}
		}

		public override void Update() {
			base.Update();

			if (!target) {
				return;
			}

			Vector3 lookDir = target.position - transform.position;

			switch (lookAxis) {
				case Axis.X:
					lookDir.x = 0;
					break;
				case Axis.Y:
					lookDir.y = 0;
					break;
				case Axis.Z:
					lookDir.z = 0;
					break;
			}

			transform.rotation = Quaternion.LookRotation(lookDir);

			if (doClamping) {
				Vector3 e = applyLocally ? transform.localEulerAngles : transform.eulerAngles;

				switch (lookAxis) {
					case Axis.X:
						e.x = Mathf.Clamp(e.x, clampMin, clampMax);
						break;
					case Axis.Y:
						e.y = Mathf.Clamp(e.y, clampMin, clampMax);
						break;
					case Axis.Z:
						e.z = Mathf.Clamp(e.z, clampMin, clampMax);
						break;
				}

				if (applyLocally) {
					transform.localEulerAngles = e;
				}else {
					transform.eulerAngles = e;
				}
			}
		}
	}
}