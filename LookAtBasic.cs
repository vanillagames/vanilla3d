﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	public class LookAtBasic : VanillaBehaviour {

		public Transform target;

		void OnEnable() {
			if (!target) {
				Log("No target set, so there's nothing to look at.");
			}
		}

		public virtual void Update() {
			if (target) {
				transform.LookAt(target);
			}
		}
	}
}
