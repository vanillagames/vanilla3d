﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This class will apply a constant force in the specified Vector3 direction. Whether that direction is applied locally or globally and
	/// in what measure will depend on the nominated VanillaForceMode. Strength and deltaTime are applied.
	/// </summary>
	public class PhysicsForceDirection : PhysicsForce {

		public Vector3 direction;

		public override Vector3 EstablishVelocity() {
			return direction * Time.deltaTime * strength;
		}
	}
}