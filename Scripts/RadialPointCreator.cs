﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Vanilla;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(RadialPointCreator))]
public class NewVanillaBehaviourEditor : Editor {

	public RadialPointCreator script;

	void OnEnable() {
		script = (RadialPointCreator)target;
	}

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		if (GUILayout.Button("Update Points")) {
			script.CreatePoints();
		}

		if (GUILayout.Button("Copy to Line Renderer Local")) {
			script.CopyPointsToAttachedLineRenderer(true);
		}

		if (GUILayout.Button("Copy to Line Renderer Global")) {
			script.CopyPointsToAttachedLineRenderer(false);
		}

		if (GUILayout.Button("Disperse Child Objects")) {
			script.DisperseChildren();
		}
	}
}
#endif

[ExecuteInEditMode]
public class RadialPointCreator : VanillaBehaviour {

	public int pointTotal;
	public float pointDistance;

	public Transform startAnchor;
	public Transform endAnchor;

	public Space pointSpace;

	public enum RadialPointsLiveMode {
		None,
		PointsOnly,
		LineRendererDirect,
		LineRendererLocalConversion,
		LineRendererGlobalConversion,
		ChildTransforms,
		LineAndChildrenDirect,
		LineAndChildrenLocalConversion,
		LineAndChildrenGlobalConversion
	}
	public RadialPointsLiveMode liveMode;

	public bool alignChildTransformRotations;

	public bool doGizmos;

	public float gizmoRadius = 0.1f;

	Transform t; // Temp cache for Transform handling

	public List<Vector3> points;

	void Update() {
		if (!startAnchor || !endAnchor) {
			return;
		}

		switch (liveMode) {
			case RadialPointsLiveMode.PointsOnly:
				CreatePoints();
				break;
			case RadialPointsLiveMode.LineRendererDirect:
				CreatePoints();
				CopyPointsToAttachedLineRenderer();
				break;
			case RadialPointsLiveMode.LineRendererLocalConversion:
				CreatePoints();
				CopyPointsToAttachedLineRenderer(true);
				break;
			case RadialPointsLiveMode.LineRendererGlobalConversion:
				CreatePoints();
				CopyPointsToAttachedLineRenderer(false);
				break;
			case RadialPointsLiveMode.ChildTransforms:
				CreatePoints();
				DisperseChildren();
				break;
			case RadialPointsLiveMode.LineAndChildrenDirect:
				CreatePoints();
				CopyPointsToAttachedLineRenderer();
				DisperseChildren();
				break;
			case RadialPointsLiveMode.LineAndChildrenLocalConversion:
				CreatePoints();
				CopyPointsToAttachedLineRenderer(true);
				DisperseChildren();
				break;
			case RadialPointsLiveMode.LineAndChildrenGlobalConversion:
				CreatePoints();
				CopyPointsToAttachedLineRenderer(false);
				DisperseChildren();
				break;
		}
	}

	public void CreatePoints() {
		points.Clear();

		float split = 1.0f / (pointTotal-1);

		if (pointSpace == Space.Self) {
			for (int i = 0; i < pointTotal; i++) {
				points.Add(transform.TransformPoint(Vector3.Slerp(transform.InverseTransformPoint(startAnchor.position), transform.InverseTransformPoint(endAnchor.position), split * i)));
			}
		} else {
			for (int i = 0; i < pointTotal; i++) {
				points.Add(Vector3.Slerp(startAnchor.position, endAnchor.position, split * i));
			}
		}
	}

	public void CopyPointsToAttachedLineRenderer() {
		if (!GetComponent<LineRenderer>()) {
			return;
		}

		LineRenderer line = GetComponent<LineRenderer>();

		line.positionCount = pointTotal;

		line.SetPositions(points.ToArray());
	}

	public void CopyPointsToAttachedLineRenderer(bool local) {
		if (!GetComponent<LineRenderer>()) {
			return;
		}

		LineRenderer line = GetComponent<LineRenderer>();

		line.positionCount = pointTotal;

		Vector3[] convertedPoints = new Vector3[pointTotal];

		if (local) {
			for (int i = 0; i < pointTotal; i++) {
				convertedPoints[i] = transform.InverseTransformPoint(points[i]);
			}
		} else {
			for (int i = 0; i < pointTotal; i++) {
				convertedPoints[i] = transform.TransformPoint(points[i]);
			}
		}

		line.SetPositions(convertedPoints);
	}

	public void DisperseChildren() {
		int p = 0;

		float split = 1.0f / (pointTotal - 1);

		int childCount = transform.childCount;
		
		//Log("childCount [{0}]", childCount);

		for (int i = 0; i < childCount; i++) {
			t = transform.GetChild(i);
			
			//Log("i [{0}] p [{1}] t [{2}]", i, p, t);

			if (t != startAnchor && t != endAnchor && t != startAnchor.parent && t != endAnchor.parent) {
				t.position = points[p];

				if (alignChildTransformRotations) {
					t.rotation = Quaternion.Lerp(startAnchor.rotation, endAnchor.rotation, p * split);
				}

				p++;

				if (p >= pointTotal) {
					return;
				}
			}
		}
	}

	public void OnDrawGizmos() {
		if (!doGizmos || !startAnchor || !endAnchor) {
			return;
		}

		Gizmos.color = Color.green;

		Gizmos.DrawSphere(transform.position, gizmoRadius * 4.0f);

		Gizmos.color = Color.black;

		if (pointSpace == Space.Self) {
			Gizmos.DrawSphere(startAnchor.position, gizmoRadius * 2.0f);

			Gizmos.DrawSphere(endAnchor.position, gizmoRadius * 2.0f);
		} else {
			Gizmos.DrawSphere(startAnchor.position, gizmoRadius * 2.0f);

			Gizmos.DrawSphere(endAnchor.position, gizmoRadius * 2.0f);
		}

		Gizmos.color = Color.white;
		
		for (int i = 0; i < points.Count; i++) {
			Gizmos.DrawSphere(points[i], gizmoRadius);
		}
	}
}