﻿using UnityEngine;
using System.Collections;

namespace Vanilla.Debugging {
	public class DebugMouseLook : VanillaDebugBase {

		public float xSensitivity;
		public float ySensitivity;

		public override void Awake() {
			base.Awake();
		}

		public override void OnEnable() {
			base.OnEnable();
		}

		void Update() {
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x + -(UnityEngine.Input.GetAxis("Mouse Y") * ySensitivity), transform.localEulerAngles.y + (UnityEngine.Input.GetAxis("Mouse X") * xSensitivity), 0);
		}
	}
}