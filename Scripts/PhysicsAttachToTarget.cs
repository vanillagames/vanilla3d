﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This is the base class for something attaching to something else physically. It splits off into a number of physics-joint-based classes that all use the 'Attach' call to do their thing.
	/// </summary>
	public class PhysicsAttachToTarget : PhysicsBase {

		[Header("Settings")]
		public bool matchTargetPositionBeforeAttach = true;
		public bool matchTargetRotationBeforeAttach = true;

		public bool childSelfToTargetWhenAttaching;

		[Header("Status")]
		[ReadOnly]
		public bool attached;

		[Header("References")]
		public Transform _target;
		public Transform target {
			get {
				if (!_target) {
					FindTarget();

					if (!_target) {
						Error("No target is set and none could be found.");
					}
				}

				return _target;
			}
			set {
				_target = value;
			}
		}

		public Rigidbody _targetRigidbody;
		public Rigidbody targetRigidbody {
			get {
				if (!_targetRigidbody) {
					FindTargetRigidbody();

					if (!_targetRigidbody) {
						Error("No target rigidbody is set and none could be found.");
					}
				}

				return _targetRigidbody;
			}
			set {
				_targetRigidbody = value;
			}
		}

		Joint _joint;
		Joint joint {
			get {
				if (!_joint) {
					_joint = GetComponent<Joint>();
				}

				return _joint;
			}
			set {
				if (_joint) {
					Destroy(_joint);
				}

				_joint = value;
			}
		}

		public void Attach(Transform newTarget) {
			 if (attached) {
				Warning("I can't attach because I'm already attached.");
				return;
			}

			target = newTarget;

			if (!target) {
				return;
			}

			if (!targetRigidbody) {
				return;
			}

			if (matchTargetPositionBeforeAttach) {
				transform.position = target.position;
				rb.MovePosition(target.position);
			}

			if (matchTargetRotationBeforeAttach) {
				transform.rotation = target.rotation;
				rb.MoveRotation(target.rotation);
			}

			if (childSelfToTargetWhenAttaching) {
				transform.SetParent(target);
			}

			joint = gameObject.AddComponent<FixedJoint>();

			joint.connectedBody = targetRigidbody;

			attached = true;
		}

		public void Detach() {
			joint = null;
			attached = false;
		}

		public virtual void FindTarget() {}

		public void FindTargetRigidbody() {
			Log("I have a target? [{0}]", target);
			Log("Target has a bod? [{0}]", target.GetComponent<Rigidbody>());

			if (target) {
				targetRigidbody = target.GetComponent<Rigidbody>();
			}
		}
	}
}