﻿using UnityEngine;

using System;

using Vanilla.VR;

namespace Vanilla.EventSystems {
	public class WorldPointerAll : BasePointer {

		[Header("[ VR Pointer ]")]
		[SerializeField]
		public WorldPointerAllModule worldPointer;

		public override void Awake() {
			base.Awake();

			worldPointer.AssignPointer(this);
		}

		void Update() {
			worldPointer.FireRay(pointerEventData);
		}
	}
}