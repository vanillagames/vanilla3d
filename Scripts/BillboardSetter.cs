﻿using UnityEngine;

using System.Collections.Generic;

namespace Vanilla.ThreeDee {
	public class BillboardSetter : VanillaBehaviour {

		public CommonUnityFunction executeOn;

		//public static Transform cameraTransform;
		public static List<BillboardListener> listeners = new List<BillboardListener>();

		Vector3 eulers;
		Vector3 flippedEulers;

		void FixedUpdate() {
			if (executeOn == CommonUnityFunction.FixedUpdate) {
				RotateListeners();
			}
		}

		void Update() {
			if (executeOn == CommonUnityFunction.Update) {
				RotateListeners();
			}
		}

		void LateUpdate() {
			if (executeOn == CommonUnityFunction.LateUpdate) {
				RotateListeners();
			}
		}

		//void RotateListeners() {
		//	switch (billboardMode) {
		//		case VanillaBillboardMode.CopyEulers:
		//			Vector3 eulers = transform.eulerAngles;

		//			for (int i = 0; i < listeners.Count; i++) {
		//				listeners[i].eulerAngles = eulers;
		//			}
		//			break;
		//		case VanillaBillboardMode.FlipEulers:
		//			Vector3 flippedEulers = transform.eulerAngles + (transform.up * 180.0f);

		//			for (int i = 0; i < listeners.Count; i++) {
		//				listeners[i].eulerAngles = flippedEulers;
		//			}
		//			break;
		//		case VanillaBillboardMode.LookAt:
		//			for (int i = 0; i < listeners.Count; i++) {
		//				listeners[i].LookAt(transform);
		//			}
		//			break;
		//	}

			void RotateListeners() {
				eulers = transform.eulerAngles;
				flippedEulers = eulers + (transform.up * 180.0f);

				for (int i = 0; i < listeners.Count; i++) {
				switch (listeners[i].billboardMode) {
					case VanillaBillboardMode.CopyEulers:
						listeners[i].transform.eulerAngles = eulers;
						break;
					case VanillaBillboardMode.FlipEulers:
						listeners[i].transform.eulerAngles = flippedEulers;
						break;
					case VanillaBillboardMode.LookAt:
						listeners[i].transform.LookAt(transform);
						break;
				}
			}
		}
	}
}