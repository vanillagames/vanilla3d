﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

namespace Vanilla.ThreeDee {

	public enum AttachMoveType {
		Fixed, // Perfect movement transferral, great for anything that doesn't have joints
		Influence, // A soft kind of force application, it pulls the grabbed object in the hands direction each FixedUpdate frame.
		None,
	}

	public enum AttachRotateType {
		RotateWithHand,
		RotateToMatchHand,
		RotateDebugTest,
		ApplyTorque,
		RotateToFaceHand,
		None,
	}

	// This is really nice! Have a look at implementation in PhysicsChase.cs
	/// <summary>
	/// Describes the 9 current ways of applying forces using either Rigidbody.AddForce or Rigidbody.AddTorque.
	/// </summary>
	public enum VanillaForceMode {
		SetDirectly,
		AddGraduallyWithMass,
		AddGraduallyWithoutMass,
		AddInstantlyWithMass,
		AddInstantlyWithoutMass,
		AddGraduallyWithMassLocal,
		AddGraduallyWithoutMassLocal,
		AddInstantlyWithMassLocal,
		AddInstantlyWithoutMassLocal,
		MoveRigidbody
	}

	public enum VanillaBillboardMode {
		CopyEulers,
		FlipEulers,
		LookAt
	}

	public static class Vanilla3D {
		///--------------------------------------------------------------------------------------------------------------------------------------------------
		/// Input -> Vector Conversion
		///--------------------------------------------------------------------------------------------------------------------------------------------------

		/// <summary>
		/// <param name="t"/>The transform to get the relative forward direction of. Theoretically, this could be any transform, but is most likely applicable for getting a direction relative between a camera and some inputs.</param>
		/// <param name="inputX">The horizontal input from a joystick, directional pad or similar input method. Note that it is often ideal to filter this input before processing here by ensuring it isn't zero.</param>
		/// <param name="inputY"/>The vertical input from a joystick, directional pad or similar input method. Note that it is often ideal to filter this input before processing here by ensuring it isn't zero.</param>
		/// </summary>
		public static Vector3 GetDirectionVectorRelativeToTransform(Transform t, float inputX, float inputY) {
			return Quaternion.AngleAxis(t.eulerAngles.y, Vector3.up) * new Vector3(inputX, 0, inputY);
		}

		public static float GetRotationAngleRelativeToTransform(Transform t, float inputX, float inputY) {
			return Mathf.Atan2(inputX, inputY) * Mathf.Rad2Deg + t.eulerAngles.y;
		}

		public static Quaternion GetRotationRelativeToTransform(Transform t, float inputX, float inputY) {
			return Quaternion.Euler(new Vector3(0, GetRotationAngleRelativeToTransform(t, inputX, inputY), 0));
		}
	}

	///--------------------------------------------------------------------------------------------------------------------------------------------------
	/// Interfaces
	///--------------------------------------------------------------------------------------------------------------------------------------------------

	public interface IPhysicsForceMode {
		VanillaForceMode forceMode();
	}

	public interface IPhysicsHasTarget {
		Transform target();
	}
}