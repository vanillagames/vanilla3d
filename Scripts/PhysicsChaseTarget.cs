﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This class will apply force in the direction of a target transform. It is not advised to apply this force locally, as it will produce very
	/// strange and unexpected results. Strength and deltaTime are applied.
	/// </summary>
	public class PhysicsChaseTarget : PhysicsForce {

		public Transform target;

		//public enum Test {
		//	Orig,
		//	OrigNormalized,
		//	OrigMultiplied,
		//	OrigNormalizedAndMultiplied
		//}

		//public Test test;

		public override Vector3 EstablishVelocity() {
			return target ? (target.position - transform.position) / Time.deltaTime * strength : Vector3.zero;

			// Orig works perfectly, the rest broke things.
			//switch (test) {
			//	case Test.Orig:
			//		return target ? (target.position - transform.position) / Time.deltaTime * strength : Vector3.zero;
			//	case Test.OrigNormalized:
			//		return target ? (target.position - transform.position).normalized / Time.deltaTime * strength : Vector3.zero;
			//	case Test.OrigMultiplied:
			//		return target ? (target.position - transform.position) * Time.deltaTime * strength : Vector3.zero;
			//	case Test.OrigNormalizedAndMultiplied:
			//		return target ? (target.position - transform.position).normalized * Time.deltaTime * strength : Vector3.zero;
			//	default:
			//		return Vector3.zero;
			//}
		}
	}
}