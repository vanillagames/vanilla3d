﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This class will apply the target euler angles to the attached rigidbody via MoveRotation and Quaternion.Euler. 
	/// Strength and deltaTime are applied.
	/// </summary>
	public class PhysicsRotationEulers : PhysicsRotation {
		public Vector3 targetEulers;

		public override Quaternion EstablishRotation() {
			return Quaternion.Euler(targetEulers * Time.deltaTime * strength);
		}
	}
}