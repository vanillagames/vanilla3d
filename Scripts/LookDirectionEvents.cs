﻿//using UnityEngine;
//using UnityEngine.Events;

//using System;
//using System.Collections;
//using System.Collections.Generic;

//using Vanilla.CustomEvents;

//namespace Vanilla.ThreeDee {
//	public class LookDirectionEvents : VanillaBehaviour {

//		[Header("Settings")]
//		[Tooltip("Which transform should we check with? If none is populated, we'll use the transform attached to this GameObject.")]
//		public Transform lookTransform;

//		[Tooltip("Which transform should we compare against? If none is populated, we'll use global directions.")]
//		public Transform comparisonTransform;

//		[Tooltip("How frequently should we check the direction of the lookTransform? A higher value means longer waits between checks.")]
//		public float timeBetweenChecks = 1.0f;

//		[Tooltip("How lenient should we be when testing the direction? A value of 0.1f would mean almost perfect while 2 would be complete.")]
//		[Range(0.02f, 1.0f)]
//		public float angleLeniency = 0.25f;

//		[Tooltip("What direction is the lookTransform currently facing?")]
//		public Direction currentDirection;

//		[Header("Which directions to test for?")]
//		public bool[] directionsToTest = new bool[6];

//		[Serializable]
//		public struct DirectionEventHandler {
//			public Direction targetDirection;
//			public DirectionEvent directionEvent;
//		}

//		[Tooltip("The events we ended up subscribing for.")]
//		public DirectionEventHandler[] directionEventHandlers = new DirectionEventHandler[6];

//		float timer;
//		Vector3 dirA;
//		Vector3 dirB;

//		void Awake() {
//			// If no lookTransform is populated, lets use the attached transform.
//			if (!lookTransform) {
//				lookTransform = transform;
//			}

//			// If no comparisonTransform is populated, lets create one that faces global directions and compare with that.
//			if (!comparisonTransform) {
//				comparisonTransform = new GameObject().transform;
//				comparisonTransform.gameObject.name = "LookDirection Comparison Transform";
//				comparisonTransform.position = Vector3.zero;
//				comparisonTransform.eulerAngles = Vector3.zero;
//				VanillaSceneLoader.i.MoveObjectToScene(comparisonTransform.gameObject, gameObject.scene.name);
//			}

//			timer = timeBetweenChecks;
//		}

//		void OnEnable() {
//			bool atLeastOneEventOrganised = false;

//			for (int i = 0; i < 6; i++) {
//				if (directionsToTest[i]) {
//					RegisterDirectionEvent(i);
//					atLeastOneEventOrganised = true;
//				}
//			}

//			if (!atLeastOneEventOrganised) {
//				Error("After running initialization, it seems that no direction events have been organized. This may be intentional, but as a precaution, this script will be disabled.");
//				enabled = false;
//			}
//		}

//		void OnDisable() {
//			// Do you need to unregister listeners if the lookRotation/this gameObject gets disabled?
//			//for (int i = 0; i < 6; i++) {
//			//	if (directionsToTest[i]) {
//			//		RegisterDirectionEvent(i);
//			//		directionEventHandlers[i].directionEvent;
//			//	}
//			//}
//		}

//		void RegisterDirectionEvent(int ID) {
//			DirectionEventHandler newHandler = new DirectionEventHandler();
//			newHandler.targetDirection = (Direction)ID;
//			newHandler.directionEvent = new DirectionEvent();

//			Log("Registering event for direction [" + newHandler.targetDirection.ToString() + "]");

//			directionEventHandlers[ID] = newHandler;
//		}

//		void Update() {
//			if (timer > 0) {
//				//Log("Waiting");
//				timer -= Time.deltaTime;
//				return;
//			} else {
//				Log("Running all requested tests...");
//				timer = timeBetweenChecks;
//			}

//			//if (Time.time % timeBetweenChecks != 0) {
//			//	return;
//			//}

//			for (int i = 0; i < 6; i++) {
//				Log("wat [" + directionEventHandlers[i].targetDirection.ToString() + "]");

//				if (directionsToTest[i]) {
//					if (currentDirection != directionEventHandlers[i].targetDirection) {
//						Direction targetDirection = directionEventHandlers[i].targetDirection;

//						Log("Running test for [" + targetDirection.ToString() + "]...");

//						// We cache the directions we're testing here for readability.
//						SetComparisonDirections(targetDirection);

//						// DirA is the relevant direction vector of the lookTransform, DirB is the relevant direction vector of the comparisonTransform.
//						if (Vector3.Dot(dirA, dirB) > 1 - angleLeniency) {
//							Log("Success! The lookTransform [" + lookTransform.gameObject.name + "] began looking in the direction [" + targetDirection.ToString() + "]");
//							currentDirection = targetDirection;
//							directionEventHandlers[i].directionEvent.Invoke(currentDirection);
//						} else {
//							Error("Failure. The lookTransform [" + lookTransform.gameObject.name + "] is not looking in the direction [" + targetDirection.ToString() + "]");
//						}
//					} else {
//						Warning("The lookTransform is already facing the direction for this test [" + directionEventHandlers[i].targetDirection.ToString() + "] so no further testing will be conducted.");
//					}
//				} else {
//					Warning("Not scheduled to test [" + i + "]");
//				}
//			}
//		}

//		void SetComparisonDirections(Direction dir) {
//			switch (dir) {
//				case Direction.Right:
//					dirA = lookTransform.right;
//					dirB = comparisonTransform.right;
//					break;
//				case Direction.Left:
//					dirA = -lookTransform.right;
//					dirB = -comparisonTransform.right;
//					break;
//				case Direction.Up:
//					dirA = lookTransform.up;
//					dirB = comparisonTransform.up;
//					break;
//				case Direction.Down:
//					dirA = -lookTransform.up;
//					dirB = -comparisonTransform.up;
//					break;
//				case Direction.Forward:
//					dirA = lookTransform.forward;
//					dirB = comparisonTransform.forward;
//					break;
//				case Direction.Back:
//					dirA = -lookTransform.forward;
//					dirB = -comparisonTransform.forward;
//					break;
//			}
//		}
//	}
//}