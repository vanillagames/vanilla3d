﻿//using UnityEngine;

//using System;
//using System.Collections;
//using System.Collections.Generic;

//using Vanilla.VR;
//using Vanilla.Math3D;
//using Vanilla.Inputs;

//namespace Vanilla.Vanilla3D {

//#if UNITY_EDITOR
//	using UnityEditor;

//	[CustomEditor(typeof(Attach))]
//	public class AttachEditor : VanillaEditor {

//		public Attach script;

//		public void OnEnable() {
//			script = (Attach)target;
//		}

//		public override void OnInspectorGUI() {
//			InspectorGUIBegin();

//			if (GUILayout.Button("Attach")) {
//				script.AttachWithinRadius();
//			}

//			if (GUILayout.Button("Let go")) {
//				script.Detach();
//			}

//			InspectorGUIEnd();
//		}
//	}
//#endif

//	public class Attach : VanillaBehaviour {

//		VRController controller;
//		Rigidbody rb;

//		public float grabRadius = 0.15f;

//		[ReadOnly]
//		public Collider[] grabResults = new Collider[5];

//		[HideInInspector]
//		public List<Attachable> grabbables;

//		[HideInInspector]
//		public Vector3 deltaDirection;

//		[HideInInspector]
//		public Vector3 deltaPosition;

//		[HideInInspector]
//		public Quaternion snapshotOfHandRotation;

//		[HideInInspector]
//		public Quaternion targetRotation;

//		Quaternion deltaRotation;

//		float angle;

//		Vector3 axis;

//		[HideInInspector]
//		public List<Quaternion> grabbableRotationSnapshots;

//		[HideInInspector]
//		public List<Vector3> grabPoints;

//		void Awake() {
//			controller = GetComponent<VRController>();

//			grabbables = new List<Attachable>();

//			grabPoints = new List<Vector3>();

//			if (rb = GetComponent<Rigidbody>()) {
//				rb = GetComponent<Rigidbody>();
//			} else {
//				rb = gameObject.AddComponent<Rigidbody>();
//				rb.isKinematic = true;
//				rb.useGravity = false;
//			}
//		}

//		//void OnEnable() {
//		//	if (controller) {
//		//		controller.axisDictionary["Grip"].onPolarityChange.AddListener(GripClickListener);
//		//		//controller.gripClick.onButtonEvent.AddListener(GripClickListener);
//		//	} else {
//		//		Error("No GenericVRController component is attached, so there's no way to trigger Attachs.");
//		//	}
//		//}

//		//void OnDisable() {
//		//	if (controller) {
//		//		controller.axisDictionary["Grip"].onPolarityChange.RemoveListener(GripClickListener);
//		//		//controller.gripClick.onButtonEvent.RemoveListener(GripClickListener);
//		//	}
//		//}

//		/*
//		public void GripClickListener(ButtonState clickState) {
//			switch (clickState) {
//				case ButtonState.Pressed:
//					AttachWithinRadius();
//					break;
//				case ButtonState.Released:
//					Detach();
//					break;
//			}
//		}
//		*/

//		//public void GripClickListener(Polarity polarity) {
//		//	switch (polarity) {
//		//		case Polarity.Positive:
//		//			AttachWithinRadius();
//		//			break;
//		//		case Polarity.Neutral:
//		//			Detach();
//		//			break;
//		//	}
//		//}

//		public void AttachWithinRadius() {
//			bool somethingGrabbed = false;

//			Physics.OverlapSphereNonAlloc(transform.position, grabRadius, grabResults);

//			for (int i = 0; i < grabResults.Length; i++) {
//				if (grabResults[i] != null) {
//					if (grabResults[i].CompareTag("Grabbable")) {
//						HandleAttachableObject(grabResults[i].GetComponent<Attachable>());
//						somethingGrabbed = true;
//					}
//				}
//			}

//			if (somethingGrabbed) {
//				snapshotOfHandRotation = transform.rotation;
//			}
//		}

//		void HandleAttachableObject(Attachable attachable) {
//			Log("Attachable object in range! [" + attachable.transform.name + "]");

//			if (!attachable.allowAttach) {
//				return;
//			}

//			attachable.Attach(this);

//			grabPoints.Add(transform.InverseTransformPoint(attachable.transform.position));

//			grabbables.Add(attachable);

//			grabbableRotationSnapshots.Add(attachable.transform.rotation); // This works 'perfectly' as long as the grabbable has no rotation. Hmm

//			//grabbableRotationSnapshots.Add(Quaternion.identity);
//		}

//		public void Detach() {
//			//Log("Let go!");

//			for (int i = 0; i < grabbables.Count; i++) {
//				//grabbables[i].rb.velocity = transform.position - grabbables[i].transform.position; // Nope
//				//grabbables[i].rb.MovePosition(transform.TransformPoint(grabPoints[i])); // Nope

//				// Not really a fan of this either, it's sorta 'faking it'.
//				// Ideally, when you Attach a grabbale, any joints would cease their influence until 'let go' again.
//				// You can't store a joints setting unless you do a big manual field population, which would suck but maybe work.
//				// You can't disable/enable a joint, that would just be silly wouldn't it
//				// You can't go rigidbody.isKinematic
//				// Might have to go with big manual field population. For example, on the spring joint, setting spring, drag and angulardrag on the RB all to 0 is basically 'off'.
//				// Sliders don't need even that, but if one had a motor, you could store the drive value instead of spring.
//				// Hinge might be a same deal, you only need the motor value if any.

//				//grabbables[i].rb.AddForce(deltaDirection * 3000.0f);

//				//switch (VanillaXR.i.XRDeviceFamily) {
//				//	case XRDeviceFamily.Oculus:
//				//		//grabbables[i].rb.velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
//				//		break;
//				//}

//				grabbables[i].Detach();
//			}

//			for (int i = 0; i < grabResults.Length; i++) {
//				grabResults[i] = null;
//			}

//			grabbableRotationSnapshots.Clear();
//			grabbables.Clear();

//			//for (int i = grabJoints.Count-1; i > -1; i--) {
//			//	Log("Destroy joint ID " + i);
//			//	Destroy(grabJoints[i]);
//			//}

//			//grabJoints.Clear();

//			grabPoints.Clear();
//		}

//		void FixedUpdate() {
//			deltaDirection = transform.position - deltaPosition;
//			deltaPosition = transform.position;

//			// What we do here is figure out this current frames rotational difference between a rotation from somewhere in time (in this instance, when we started the Attach)
//			// In order to get that quaternion difference, we multiply the current rotation by the inverse of the snapshot in time.
//			// Theoretically, you could use the rotation from the previous frame in order to get a true frame-by-frame delta rotation.

//			for (int i = 0; i < grabbables.Count; i++) {
//				///--------------------------------------------------------------------------------------------------------------------------------------------------------------------
//				/// Handle Movement
//				///--------------------------------------------------------------------------------------------------------------------------------------------------------------------

//				switch (grabbables[i].moveType) {
//					case AttachMoveType.Fixed:
//						grabbables[i].rb.MovePosition(transform.TransformPoint(grabPoints[i]));
//						break;
//					case AttachMoveType.Influence:
//						Vector3 velocityTarget = (transform.TransformPoint(grabPoints[i]) - grabbables[i].transform.position) / Time.fixedDeltaTime;

//						grabbables[i].rb.velocity = Vector3.MoveTowards(grabbables[i].rb.velocity, velocityTarget, 10f);
//						break;
//				}

//				///--------------------------------------------------------------------------------------------------------------------------------------------------------------------
//				/// Handle Rotation
//				///--------------------------------------------------------------------------------------------------------------------------------------------------------------------

//				switch (grabbables[i].rotateType) {
//					case AttachRotateType.RotateWithHand:
//						// Neither of these work, but MatchHand does. Hm...
//						//deltaRotation = VanillaMath.GetQuaternionDifference(grabbableRotationSnapshots[i], grabbables[i].transform.rotation);
//						//deltaRotation = VanillaMath.GetQuaternionDifference(snapshotOfHandRotation, grabbables[i].transform.rotation);

//						deltaRotation.ToAngleAxis(out angle, out axis);

//						if (angle > 180) {
//							angle -= 360;
//						}

//						if (angle != 0) {
//							Vector3 angularTarget = angle * axis;
//							grabbables[i].rb.angularVelocity = Vector3.MoveTowards(grabbables[i].rb.angularVelocity, angularTarget, 10f * (Time.fixedDeltaTime * 4000));
//						}
//						break;

//					case AttachRotateType.RotateToMatchHand:
//						targetRotation = transform.rotation;

//						deltaRotation = VanillaMath3D.GetQuaternionDifference(targetRotation, grabbables[i].transform.rotation);

//						deltaRotation.ToAngleAxis(out angle, out axis);

//						if (angle > 180) {
//							angle -= 360;
//						}

//						if (angle != 0) {
//							Vector3 angularTarget = angle * axis;
//							grabbables[i].rb.angularVelocity = Vector3.MoveTowards(grabbables[i].rb.angularVelocity, angularTarget, 10f * (Time.fixedDeltaTime * 4000));
//						}
//						break;
//					case AttachRotateType.RotateDebugTest:
//						//Quaternion deltaRotationDifference = transform.rotation * Quaternion.Inverse(deltaRotation);

//						//grabbables[i].rb.MoveRotation(grabbables[i].transform.rotation * deltaRotationDifference);
//						//grabbables[i].rb.MoveRotation(grabbableRotationSnapshots[i] * deltaRotationDifference);

//						//deltaRotation = transform.rotation;

//						///--------

//						//Quaternion deltaQuaternionAngle = transform.rotation * Quaternion.Inverse(deltaRotation);
//						//deltaRotation = transform.rotation;

//						//grabbables[i].rb.MoveRotation(grabbables[i].rb.rotation * deltaQuaternionAngle);

//						///

//						Quaternion deltaRotationDifference = transform.rotation * Quaternion.Inverse(snapshotOfHandRotation);

//						grabbables[i].rb.MoveRotation(grabbableRotationSnapshots[i] * deltaRotationDifference);

//						break;

//					case AttachRotateType.ApplyTorque:
//						//deltaRotation = VanillaMath.GetQuaternionDifference(transform.rotation, grabbables[i].transform.rotation);

//						//grabbables[i].rb.AddRelativeTorque(VanillaMath.GetQuaternionDifference(transform.rotation, deltaRotation).eulerAngles, ForceMode.Force);
//						//deltaRotation = transform.rotation;
//						break;

//					case AttachRotateType.RotateToFaceHand:
//						if (grabbables[i].joint) {
//							grabbables[i].rb.MoveRotation(Quaternion.Euler(Vector3.Scale(transform.position - grabbables[i].transform.position, grabbables[i].joint.axis)));
//						}
//						break;

//					case AttachRotateType.None:

//						break;
//				}
//			}
//		}

//		//void OnDrawGizmos() {
//		//	if (Application.isPlaying) {
//		//		Gizmos.color = Color.green;
//		//		//Gizmos.DrawLine(transform.position, transform.position + (deltaDirection));
//		//		//Gizmos.DrawRay(transform.position, deltaDirection);

//		//		for (int i = 0; i < grabPoints.Count; i++) {
//		//			//Gizmos.DrawSphere(grabbables[i].transform.position + grabbables[i].transform.TransformPoint(grabPoints[i]), 0.025f);
//		//			Gizmos.DrawSphere(transform.TransformPoint(grabPoints[i]), 0.025f);
//		//		}
//		//	}
//		//}
//	}
//}