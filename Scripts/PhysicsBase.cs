﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This is the base physics class within Vanilla. It only features a uniform 'strength' float which can be considered an agnostic modifier for
	/// all physics applications.
	/// </summary>
	[RequireComponent(typeof(Rigidbody))]
	public class PhysicsBase : VanillaBehaviour {

		public float strength = 1.0f;

		[HideInInspector]
		public Rigidbody _rb;
		public Rigidbody rb {
			get {
				if (!_rb) {
					_rb = GetComponent<Rigidbody>();
				}

				return _rb;
			}
		}
	}
}