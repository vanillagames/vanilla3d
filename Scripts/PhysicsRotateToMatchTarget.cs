﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This class will apply the target transforms rotation to the attached rigidbody via MoveRotation. This effectively makes the rigidbody match
	/// the targets rotation perfectly. Strength and deltaTime are applied.
	/// </summary>
	public class PhysicsRotateToMatchTarget : PhysicsRotation {
		public Transform target;

		public override Quaternion EstablishRotation() {
			if (target) {
				return target.rotation;
			} else {
				return Quaternion.identity;
			}
		}
	}
}