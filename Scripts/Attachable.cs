﻿//using UnityEngine;
//using UnityEngine.Events;

//using System;
//using System.Collections;
//using System.Collections.Generic;

//namespace Vanilla.Vanilla3D {

//	[RequireComponent(typeof(Rigidbody))]
//	public class Attachable : VanillaBehaviour {

//		[HideInInspector]
//		public Rigidbody rb;

//		[Header("Settings")]
//		[Tooltip("How should this attachable move when attached?")]
//		public AttachMoveType moveType;

//		[Tooltip("How should this attachable rotate when attached?")]
//		public AttachRotateType rotateType;

//		public bool allowAttach = true;
//		public bool disableRigidbodyDragWhileHeld;
//		public bool disableJointInfluenceWhileHeld;

//		[Header("Read Only")]
//		public Attach currentAttach;

//		public Joint joint;

//		public enum JointType {
//			Configurable,
//			Hinge,
//			Spring,
//			Fixed,
//			None
//		}
//		public JointType jointType;

//		public float dragCache, angularDragCache;
//		//public Vector3 storedVelocity, storedAngularVelocity; // Don't need these

//		//public float springCache, dampeningCache, storedDrive;
//		//public bool boolCache1, boolCache2, boolCache3, boolCache4, boolCache5;
//		//public float floatCache1, floatCache2, floatCache3, floatCache4, floatCache5, floatCache6;

//		public bool[] boolCache;
//		public float[] floatCache;
//		public Vector3[] vector3Cache;

//		public Vector3 storedAxis;

//		public UnityEvent onPickup;
//		public UnityEvent onLetGo;

//		void Awake() {
//			if (onPickup == null) {
//				onPickup = new UnityEvent();
//			}

//			if (onLetGo == null) {
//				onLetGo = new UnityEvent();
//			}

//			rb = GetComponent<Rigidbody>();
//			joint = GetComponent<Joint>();

//			EstablishJointType();

//			if (disableRigidbodyDragWhileHeld) {
//				CacheDragSettings();
//			}

//			if (disableJointInfluenceWhileHeld) {
//				CacheJointSettings();
//			}
//		}

//		void EstablishJointType() {
//			if (!joint) {
//				if (GetComponent<Joint>()) {
//					joint = GetComponent<Joint>();
//				}
//			}

//			if (joint is ConfigurableJoint) {
//				jointType = JointType.Configurable;

//			} else if (joint is HingeJoint) {
//				jointType = JointType.Hinge;

//				boolCache = new bool[2];
//			} else if (joint is SpringJoint) {
//				jointType = JointType.Spring;

//				floatCache = new float[2];
//			} else if (joint is FixedJoint) {
//				jointType = JointType.Fixed;
//			} else {
//				jointType = JointType.None;
//			}
//		}

//		public void Attach(Attach attach) {
//			currentAttach = attach;

//			if (disableRigidbodyDragWhileHeld) {
//				ZeroOutDragSettings();
//			}

//			if (disableJointInfluenceWhileHeld) {
//				ZeroOutJointSettings();
//			}

//			switch (moveType) {
//				case AttachMoveType.Fixed:
//					rb.velocity = Vector3.zero;
//					break;
//			}

//			switch (rotateType) {
//				case AttachRotateType.RotateWithHand:
//					rb.angularVelocity = Vector3.zero;
//					break;
//			}

//			onPickup.Invoke();
//		}

//		void FixedUpdate() {
//			if (!currentAttach) {
//				return;
//			}

//			/*
//			Quaternion deltaRotation = currentGrabber.transform.rotation * Quaternion.Inverse(transform.rotation);
//			Vector3 deltaPosition = (currentGrabber.transform.position - transform.position);

//			float deltaPoses = Time.fixedDeltaTime;

//			float angle;
//			Vector3 axis;
//			deltaRotation.ToAngleAxis(out angle, out axis);

//			if (angle > 180) {
//				angle -= 360;
//			}

//			if (angle != 0) {
//				Vector3 angularTarget = angle * axis;
//				rb.angularVelocity = Vector3.MoveTowards(rb.angularVelocity, angularTarget, 10f * (deltaPoses * 1000)); // Change the value 1000 to something like 2 or 5 for a bit of wobbly jelly physics, looks awesome!
//			}

//			Vector3 velocityTarget = deltaPosition / deltaPoses;

//			rb.velocity = Vector3.MoveTowards(rb.velocity, velocityTarget, 10f);
//			*/
//		}

//		public void Detach() {
//			//switch (moveType) {

//			//}

//			//switch (rotateType) {

//			//}

//			if (disableRigidbodyDragWhileHeld) {
//				ResetDragSettings();
//			}

//			if (disableJointInfluenceWhileHeld) {
//				ResetJointSettings();
//			}

//			onLetGo.Invoke();

//			currentAttach = null;
//		}

//		public void ForceLetGo() {
//			if (!currentAttach) {
//				return;
//			}

//			currentAttach.Detach();
//		}

//		void CacheDragSettings() {
//			dragCache = rb.drag;
//			angularDragCache = rb.angularDrag;
//		}

//		void ZeroOutDragSettings() {
//			rb.drag = 0;
//			rb.angularDrag = 0;
//		}

//		void ResetDragSettings() {
//			rb.drag = dragCache;
//			rb.angularDrag = angularDragCache;
//		}

//		void CacheJointSettings() {
//			switch (jointType) {
//				case JointType.Configurable:

//					break;
//				case JointType.Hinge:
//					HingeJoint hingeJoint = joint as HingeJoint;

//					boolCache[0] = hingeJoint.useSpring;
//					boolCache[1] = hingeJoint.useMotor;

//					//boolCache[2] = hingeJoint.motor.freeSpin;
//					//boolCache[3] = hingeJoint.useLimits;

//					//floatCache[0] = hingeJoint.spring.spring;
//					//floatCache[1] = hingeJoint.spring.damper;
//					//floatCache[2] = hingeJoint.spring.targetPosition;
//					//floatCache[3] = hingeJoint.motor.force;
//					//floatCache[4] = hingeJoint.motor.targetVelocity;
//					//floatCache[5] = hingeJoint.limits.min;
//					//floatCache[6] = hingeJoint.limits.max;



//					break;
//				case JointType.Spring:
//					SpringJoint springJoint = joint as SpringJoint;

//					floatCache[0] = springJoint.spring;
//					floatCache[1] = springJoint.damper;
//					//floatCache3 = springJoint.minDistance;
//					//floatCache4 = springJoint.maxDistance;
//					//floatCache5 = springJoint.tolerance;
//					break;
//				case JointType.Fixed:

//					break;
//			}
//		}

//		void ZeroOutJointSettings() {
//			switch (jointType) {
//				case JointType.Configurable:

//					break;
//				case JointType.Hinge:
//					HingeJoint hingeJoint = joint as HingeJoint;

//					hingeJoint.useSpring = false;
//					hingeJoint.useMotor = false;

//					//hingeJoint.motor.freeSpin				= 0;
//					//hingeJoint.useLimits					= 0;

//					//hingeJoint.spring.spring				= 0;  
//					//hingeJoint.spring.damper				= 0;
//					//hingeJoint.spring.targetPosition		= 0;

//					//hingeJoint.motor.force				= 0;
//					//hingeJoint.motor.targetVelocity		= 0;

//					//hingeJoint.limits.min					= 0;
//					//hingeJoint.limits.max					= 0;
//					break;
//				case JointType.Spring:
//					SpringJoint springJoint = joint as SpringJoint;

//					springJoint.spring = 0;
//					springJoint.damper = 0;

//					//springJoint.minDistance				= 0;
//					//springJoint.maxDistance				= 0;
//					//springJoint.tolerance					= 0;
//					break;
//				case JointType.Fixed:

//					break;
//			}
//		}

//		void ResetJointSettings() {
//			switch (jointType) {
//				case JointType.Configurable:

//					break;
//				case JointType.Hinge:
//					HingeJoint hingeJoint = joint as HingeJoint;

//					hingeJoint.useSpring = boolCache[0];
//					hingeJoint.useMotor = boolCache[1];

//					//hingeJoint.motor.freeSpin				= boolCache[2];
//					//hingeJoint.useLimits					= boolCache[3];

//					//hingeJoint.spring.spring				= floatCache[0];
//					//hingeJoint.spring.damper				= floatCache[1]; 
//					//hingeJoint.spring.targetPosition		= floatCache[2]; 
//					//hingeJoint.motor.force				= floatCache[3]; 
//					//hingeJoint.motor.targetVelocity		= floatCache[4]; 
//					//hingeJoint.limits.min					= floatCache[5]; 
//					//hingeJoint.limits.max					= floatCache[6]; 
//					break;
//				case JointType.Spring:
//					SpringJoint springJoint = joint as SpringJoint;

//					springJoint.spring = floatCache[0];
//					springJoint.damper = floatCache[1];

//					//springJoint.minDistance				= floatCache[2];
//					//springJoint.maxDistance				= floatCache[3];
//					//springJoint.tolerance					= floatCache[4];
//					break;
//				case JointType.Fixed:

//					break;
//			}
//		}
//	}
//}