﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This base class will apply a constant Vector3 force to the attached rigidbody. It is recommended that you modify the drag and
	/// angular drag in order to achieve smooth physics. 10 seems to be ideal at a uniform scale of 1.
	/// </summary>
	public class PhysicsForce : PhysicsBase {
		public VanillaForceMode forceMode;

		public void FixedUpdate() {
			ApplyVelocity(EstablishVelocity());
		}

		public virtual Vector3 EstablishVelocity() {
			return Vector3.zero;
		}

		public void ApplyVelocity(Vector3 force) {
			switch (forceMode) {
				case VanillaForceMode.SetDirectly:
					rb.velocity = force;
					break;
				case VanillaForceMode.AddGraduallyWithMass:
					rb.AddForce(force, ForceMode.Force);
					break;
				case VanillaForceMode.AddGraduallyWithoutMass:
					rb.AddForce(force, ForceMode.Acceleration);
					break;
				case VanillaForceMode.AddInstantlyWithMass:
					rb.AddForce(force, ForceMode.Impulse);
					break;
				case VanillaForceMode.AddInstantlyWithoutMass:
					rb.AddForce(force, ForceMode.VelocityChange);
					break;
				case VanillaForceMode.AddGraduallyWithMassLocal:
					rb.AddRelativeForce(force, ForceMode.Force);
					break;
				case VanillaForceMode.AddGraduallyWithoutMassLocal:
					rb.AddRelativeForce(force, ForceMode.Acceleration);
					break;
				case VanillaForceMode.AddInstantlyWithMassLocal:
					rb.AddRelativeForce(force, ForceMode.Impulse);
					break;
				case VanillaForceMode.AddInstantlyWithoutMassLocal:
					rb.AddRelativeForce(force, ForceMode.VelocityChange);
					break;
				case VanillaForceMode.MoveRigidbody:
					rb.MovePosition(transform.position + force);
					break;
			}
		}
	}
}