﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This base class will apply a constant Vector3 torque to the attached rigidbody. It is recommended that you modify the drag and
	/// angular drag in order to achieve smooth physics. 10 seems to be ideal at a uniform scale of 1 and strength of 0.1.
	/// </summary>
	public class PhysicsTorque : PhysicsBase {

		public VanillaForceMode forceMode;

		public void FixedUpdate() {
			ApplyTorque(EstablishTorque());
		}

		public void ApplyTorque(Vector3 torque) {
			switch (forceMode) {
				case VanillaForceMode.SetDirectly:
					rb.angularVelocity = torque;
					break;
				case VanillaForceMode.AddGraduallyWithMass:
					rb.AddTorque(torque, ForceMode.Force);
					break;
				case VanillaForceMode.AddGraduallyWithoutMass:
					rb.AddTorque(torque, ForceMode.Acceleration);
					break;
				case VanillaForceMode.AddInstantlyWithMass:
					rb.AddTorque(torque, ForceMode.Impulse);
					break;
				case VanillaForceMode.AddInstantlyWithoutMass:
					rb.AddTorque(torque, ForceMode.VelocityChange);
					break;
				case VanillaForceMode.AddGraduallyWithMassLocal:
					rb.AddRelativeTorque(torque, ForceMode.Force);
					break;
				case VanillaForceMode.AddGraduallyWithoutMassLocal:
					rb.AddRelativeTorque(torque, ForceMode.Acceleration);
					break;
				case VanillaForceMode.AddInstantlyWithMassLocal:
					rb.AddRelativeTorque(torque, ForceMode.Impulse);
					break;
				case VanillaForceMode.AddInstantlyWithoutMassLocal:
					rb.AddRelativeTorque(torque, ForceMode.VelocityChange);
					break;
			}
		}

		public virtual Vector3 EstablishTorque() {
			return Vector3.zero;
		}
	}
}