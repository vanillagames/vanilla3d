﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This class will apply a quaternion via MoveRotation to the attached rigidbody. Please note that Time.deltaTime should be applied to a
	/// rotation before using it to ensure smoothness.
	/// </summary>
	public class PhysicsRotation : PhysicsBase {

		public void FixedUpdate() {
			rb.MoveRotation(EstablishRotation());
		}

		public virtual Quaternion EstablishRotation() {
			return Quaternion.identity;
		}
	}
}