﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	public class BillboardListener : VanillaBehaviour {

		public VanillaBillboardMode billboardMode;

		public void OnBecameVisible() {
			Activate();
		}

		public void OnBecameInvisible() {
			Deactivate();
		}

		public void Activate() {
			if (!BillboardSetter.listeners.Contains(this)) {
				BillboardSetter.listeners.Add(this);
			}
		}

		public void Deactivate() {
			if (BillboardSetter.listeners.Contains(this)) {
				BillboardSetter.listeners.Remove(this);
			}
		}
	}
}