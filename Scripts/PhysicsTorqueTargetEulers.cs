﻿using UnityEngine;

namespace Vanilla.ThreeDee {
	/// <summary>
	/// This class will apply a constant source of torque to the rigidbody based on the euler angles of a target transform.
	/// Note that it will not try to match the target, but rather add the targets eulers constantly and directly.
	/// </summary>
	public class PhysicsTorqueTargetEulers : PhysicsTorque {

		public Transform target;

		public override Vector3 EstablishTorque() {
			return (target.eulerAngles * Time.deltaTime * strength);
		}
	}
}