﻿using UnityEngine;
using System.Collections;

using Vanilla;
using Vanilla.ThreeDee;
using Vanilla.CustomEvents;

public class CollisionHandler : VanillaBehaviour {

	[Header("[ Collision Handler ]")]

	[Space(10)]
	public bool ignoreCollisionsWhileDisabled;

	public enum ColliderTypeHandling {
		Collider = 1,
		Trigger = 2,
	}

	[Space(10), EnumFlags(1)]
	public ColliderTypeHandling allowedColliderTypes;

	public enum CollisionEventHandling {
		Entry = 1,
		Stay = 2,
		Exit = 4
	}

	[Space(10),EnumFlags(1)]
	public CollisionEventHandling allowedCollisionEvents;
	
	[SerializeField]
	private GameObjectEvent _onEntry;
	public GameObjectEvent onEntry {
		get {
			if (_onEntry == null) {
				_onEntry = new GameObjectEvent();
			}

			return _onEntry;
		}
	}

	[SerializeField]
	private GameObjectEvent _onExit;
	public GameObjectEvent onExit {
		get {
			if (_onExit == null) {
				_onExit = new GameObjectEvent();
			}

			return _onExit;
		}
	}

	[SerializeField]
	private GameObjectEvent _onStay;
	public GameObjectEvent onStay {
		get {
			if (_onStay == null) {
				_onStay = new GameObjectEvent();
			}

			return _onStay;
		}
	}

	public bool runExampleListeners;

	///--------------------------------------------------------------------------------------------\
	/// Entry
	///--------------------------------------------------------------------------------------------/

	public virtual void OnTriggerEnter(Collider other) {
		Log("OnTriggerEnter");

		if (allowedCollisionEvents.HasFlag(CollisionEventHandling.Entry) && allowedColliderTypes.HasFlag(ColliderTypeHandling.Trigger) && EnabledStatusIsAllowed()) {
			Log("OnTriggerEnter validation passed!");

			HandleEnter(other);
		} else {
			Log("OnTriggerEnter validation failed!");
		}
	}

	public virtual void OnCollisionEnter(Collision other) {
		Log("OnCollisionEnter");

		if (allowedCollisionEvents.HasFlag(CollisionEventHandling.Entry) && allowedColliderTypes.HasFlag(ColliderTypeHandling.Collider) && EnabledStatusIsAllowed()) {
			Log("OnCollisionEnter validation passed!");

			HandleEnter(other.collider);
		} else {
			Log("OnCollisionEnter validation failed!");
		}
	}

	///--------------------------------------------------------------------------------------------\
	/// Exit
	///--------------------------------------------------------------------------------------------/

	public virtual void OnTriggerExit(Collider other) {
		Log("OnTriggerExit");

		if (allowedCollisionEvents.HasFlag(CollisionEventHandling.Exit) && allowedColliderTypes.HasFlag(ColliderTypeHandling.Trigger) && EnabledStatusIsAllowed()) {
			Log("OnTriggerExit validation passed!");

			HandleExit(other);
		} else {
			Log("OnTriggerExit validation failed!");
		}
	}

	public virtual void OnCollisionExit(Collision other) {
		Log("OnCollisionExit");

		if (allowedCollisionEvents.HasFlag(CollisionEventHandling.Exit) && allowedColliderTypes.HasFlag(ColliderTypeHandling.Collider) && EnabledStatusIsAllowed()) {
			Log("OnCollisionExit validation passed!");

			HandleExit(other.collider);
		} else {
			Log("OnCollisionExit validation failed!");
		}
	}

	///--------------------------------------------------------------------------------------------\
	/// Stay
	///--------------------------------------------------------------------------------------------/

	public virtual void OnTriggerStay(Collider other) {
		Log("OnTriggerStay");

		if (allowedCollisionEvents.HasFlag(CollisionEventHandling.Stay) && allowedColliderTypes.HasFlag(ColliderTypeHandling.Trigger) && EnabledStatusIsAllowed()) {
			Log("OnTriggerStay validation passed!");

			HandleStay(other);
		} else {
			Log("OnTriggerStay validation failed!");
		}
	}

	public virtual void OnCollisionStay(Collision other) {
		Log("OnCollisionStay");

		if (allowedCollisionEvents.HasFlag(CollisionEventHandling.Stay) && allowedColliderTypes.HasFlag(ColliderTypeHandling.Collider) && EnabledStatusIsAllowed()) {
			Log("OnCollisionStay validation passed!");

			HandleStay(other.collider);
		} else {
			Log("OnCollisionStay validation failed!");
		}
	}

	///--------------------------------------------------------------------------------------------\
	/// Helper functions
	///--------------------------------------------------------------------------------------------/

	bool EnabledStatusIsAllowed() {
		Log("Enabled status allowed this time? [{0}]", enabled || !ignoreCollisionsWhileDisabled);

		return enabled || !ignoreCollisionsWhileDisabled;
	}

	///--------------------------------------------------------------------------------------------\
	/// Handling
	///--------------------------------------------------------------------------------------------/

	public virtual void HandleEnter(Collider other) {
		Log("Entry detected");

		onEntry.Invoke(other.gameObject);
	}

	public virtual void HandleExit(Collider other) {
		Log("Exit detected");

		onExit.Invoke(other.gameObject);
	}

	public virtual void HandleStay(Collider other) {
		Log("Stay detected");

		onStay.Invoke(other.gameObject);
	}

	///--------------------------------------------------------------------------------------------\
	/// Example Functions
	///--------------------------------------------------------------------------------------------/

	void Start() {
		if (runExampleListeners) {
			onEntry.AddListener(ObjectEntryExample);
			onExit.AddListener(ObjectExitExample);
			onStay.AddListener(ObjectStayExample);
		}

		Log("Trigger flag status [{0}]", allowedColliderTypes.HasFlag(ColliderTypeHandling.Trigger));
		Log("Collider flag status [{0}]", allowedColliderTypes.HasFlag(ColliderTypeHandling.Collider));

		Log("Entry flag status [{0}]", allowedCollisionEvents.HasFlag(CollisionEventHandling.Entry));
		Log("Exit flag status [{0}]", allowedCollisionEvents.HasFlag(CollisionEventHandling.Exit));
		Log("Stay flag status [{0}]", allowedCollisionEvents.HasFlag(CollisionEventHandling.Stay));
	}

	public void ObjectEntryExample(GameObject entryObject) {
		Log("This object just entered [{0}]", entryObject.name);
	}

	public void ObjectExitExample(GameObject exitObject) {
		Log("This object just exited [{0}]", exitObject.name);
	}

	public void ObjectStayExample(GameObject stayObject) {
		Log("This object is staying in my proximity [{0}]", stayObject.name);
	}
}